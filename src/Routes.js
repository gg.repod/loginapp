import { View, Text } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import Welcome from './Main/Welcome'
import Policy from './Main/Policy'
import Login from './Main/Login'
import Forget from './Main/Forget'
import SendOtp from './Main/SendOtp'
import Verify from './Main/Verify'
import CreatePin from './Main/CreatePin'
import SettingFinger from './Main/SettingFinger'
import LoginWithPin from './Main/LoginWithPin'
const Stack = createNativeStackNavigator()

const Routes = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen component={Welcome} name='welcome'/>
            <Stack.Screen component={Policy} name='policy'/>
            <Stack.Screen component={Login} name='login'/>
            <Stack.Screen component={Forget} name='forget'/>
            <Stack.Screen component={SendOtp} name='otp'/>
            <Stack.Screen component={Verify} name='verify'/>
            <Stack.Screen component={CreatePin} name='createpin'/>
            <Stack.Screen component={SettingFinger} name='finger'/>
            <Stack.Screen component={LoginWithPin} name='loginpin'/>
        </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Routes