import { View, Text } from "react-native";
import React, { useEffect, useState } from "react";
import * as Updates from "expo-updates";
import * as Font from "expo-font";
import Routes from "./Routes";
import color from "../color";
const Main = () => {
  const [progress, setProgress] = useState(0.0);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    initApp();
    increaseProgress();
  }, []);

  const increaseProgress = () => {
    setTimeout(() => {
      setProgress(0.2);
    }, 300);
    setTimeout(() => {
      setProgress(0.55);
    }, 600);
    setTimeout(() => {
      setProgress(0.75);
    }, 800);
    setTimeout(() => {
      setProgress(0.95);
    }, 1200);
  };
  const initApp = async () => {
    try {
      if (!__DEV__) {
        if (Platform.OS === "ios") {
          const update = await Updates.checkForUpdateAsync();
          if (update.isAvailable) {
            setChecktext("New updates available");
            await Updates.fetchUpdateAsync();
            Updates.reloadAsync();
          }
        } else {
          const update = await Updates.checkForUpdateAsync();
          if (update.isAvailable) {
            setChecktext("New updates available");
            Updates.fetchUpdateAsync();
            RNRestart.Restart();
          }
        }
      }
      await loadFont();
      setTimeout(() => {
        setLoading(true);
      }, 1500);
    } catch (error) {
      await loadFont();
      setTimeout(() => {
        setLoading(true);
      }, 1500);
    }
  };
  const loadFont = async () => {
    await Font.loadAsync({
      a: require("../assets/font/NotoSansThai-Medium.ttf"),
    });
    await Font.loadAsync({
      b: require("../assets/font/NotoSansThai-Bold.ttf"),
    });
    await Font.loadAsync({
      c: require("../assets/font/NotoSansThai-Light.ttf"),
    });
  };

  return (
    <>
      {loading ? (
        <Routes />
      ) : (
        <View style={{ flex: 1, backgroundColor:color.colorMain}}/>
      )}
    </>
  );
};

export default Main;
