import React, { useRef } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import color from '../../color';

const OTPInput = ({ onChangeText }) => {
  const inputs = Array(6).fill(0);
  const inputRefs = useRef([]);

  const handleFocus = (index) => {
    if (inputRefs.current[index] && inputRefs.current[index].focus) {
      inputRefs.current[index].focus();
    }
  };

  const handleKeyPress = (e, index) => {
    if (e.nativeEvent.key !== 'Backspace' && index < inputs.length - 1) {
      inputRefs.current[index + 1].focus();
    }
  };

  const otpInputs = inputs.map((val, index) => (
    <TextInput
      key={index}
      style={styles.input}
      onChangeText={(text) => onChangeText(index, text)}
      maxLength={1}
      keyboardType="numeric"
      ref={(input) => {
        inputRefs.current[index] = input;
      }}
      onFocus={() => handleFocus(index)}
      onKeyPress={(e) => handleKeyPress(e, index)}
    />
  ));

  return (
    <View style={styles.container}>
      {otpInputs}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    width: 40,
    height: 40,
    borderBottomWidth: 1,
    borderColor: color.gray,
    textAlign: 'center',
    fontSize: 20,
    marginHorizontal: 5,
    fontFamily:"a"
  },
});

export default OTPInput;
