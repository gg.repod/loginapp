import { View, Text, StyleSheet } from "react-native";
import React from "react";
import color from "../../color";
import i18n from "../languge/i18n";
const AppText = (props) => {
  const {
    children,
    lang,
    onPress,
    style,
    underline,
    numberOfLines,
    ellipsizeMode,
    bold,
    light,
  } = props;

  const { x, s, m, l, xl, xxl, _3xl , _4xl} = props;
  const { white,main,gray,blue} = props
  const {marginL,marginT} = props
  const {c} = props
  return (
    <Text
      style={[
        styles.textStyle,
        style,
        x && { fontSize: 12 },
        s && { fontSize:14},
        m && { fontSize: 16},
        l && { fontSize: 18 },
        xl && { fontSize:20},
        xxl && { fontSize: 22 },
        _3xl && { fontSize: 24 },
        _4xl && { fontSize: 26 },

        white && { color: color.white },
        main && {color:color.colorMain},
        gray && {color:color.gray},
        blue && {color:color.blue},

        marginL && {marginLeft:10},
        marginT && {marginTop:10},
        
        underline && { textDecorationLine: "underline" },
        bold && { fontFamily: "b" },
        light && { fontFamily: "c" },

        c && {textAlign:"center"},
      ]}
      numberOfLines={numberOfLines}
      ellipsizeMode={ellipsizeMode}
      onPress={onPress}
    >
      {lang ? i18n.t(children) : children}
    </Text>
  );
};

export default AppText;
const styles = StyleSheet.create({
  textStyle: {
    color: color.black,
    fontFamily: "a",
    textAlignVertical: "center",

  },
});
