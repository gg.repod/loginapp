import React, { createContext, useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';

export const LocaleContext = createContext()

export const LocaleProvider = ({ children }) => {
    const [localeState, setLocaleState] = useState('th')

    useEffect(() => {
        getDefalut()
    }, [])

    const getDefalut = async () => {
        const data = await AsyncStorage.getItem('locale')
        if (data !== null) {
            setLocaleState(data)
        }
    }


    return (
        <LocaleContext.Provider
            value={{
                localeState,
                setLocaleState,
                getDefalut
            }}
        >
            {children}
        </LocaleContext.Provider>
    )
}


