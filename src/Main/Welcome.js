import { StyleSheet, Text, View ,TouchableOpacity} from 'react-native'
import React,{useContext} from 'react'
import AppText from '../component/AppText'
import color from '../../color'
import i18n from '../languge/i18n'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useNavigation } from '@react-navigation/native'
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome6 } from "@expo/vector-icons";


const Welcome = () => {
  const navigation = useNavigation()

  const changeLanguage = async (language) => {
    if (language == 'th') {
      AsyncStorage.setItem('locale', 'th')
      i18n.locale = 'th'
    } else {
      AsyncStorage.setItem('locale', 'en')
      i18n.locale = 'en'
    }
    navigation.navigate("policy")

  }
  return (
    <View style={{flex:1,justifyContent:"center",padding:20}}>

      <AppText bold _3xl>ยินดีต้อนรับ</AppText>
      <AppText  l>กรุณาเลือกภาษา</AppText>
      <View style={{marginTop:100}}>
        <TouchableOpacity style={styles.button}    onPress={() => changeLanguage('en')}>
        <AppText  m white>English</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button}  onPress={() => changeLanguage('th')}>
        <AppText  m white>ไทย</AppText>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Welcome

const styles = StyleSheet.create({
  button:{
    backgroundColor:color.colorMain,
    borderRadius:6,
    alignItems:"center",
    padding:10,
    marginBottom:20
  }
})