import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import AppText from "../component/AppText";
import i18n from "../languge/i18n";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import color from "../../color";
import { useNavigation } from "@react-navigation/native";

const SettingFinger = () => {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, justifyContent: "center", padding: 20 }}>
      <AppText bold _3xl>
        Touch ID
      </AppText>
      <View style={{ width: "50%", marginTop: 10 }}>
        <AppText l>{i18n.t("setting")}</AppText>
      </View>
      <View style={{ alignItems: "center", marginTop: 125, marginBottom: 175 }}>
        <MaterialCommunityIcons
          name="fingerprint"
          size={100}
          color={color.colorMain}
          style={{ backgroundColor: "#fff", borderRadius: 100, elevation: 5 }}
        />
      </View>
      <TouchableOpacity
        style={{
          alignItems: "center",
          padding: 15,
          borderRadius: 6,
          backgroundColor: color.colorMain,
          marginTop: 50,
        }}
      >
        <AppText white>{i18n.t("settingFinger")}</AppText>
      </TouchableOpacity>
      <TouchableOpacity
        style={{ alignItems: "center", marginTop: 25 }}
        onPress={() => navigation.navigate("loginpin")}
      >
        <AppText main>{i18n.t("skip")}</AppText>
      </TouchableOpacity>
    </View>
  );
};

export default SettingFinger;
