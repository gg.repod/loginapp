import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import i18n from "../languge/i18n";
import AppText from "../component/AppText";
import { AntDesign } from "@expo/vector-icons";
import { FontAwesome6 } from "@expo/vector-icons";
import color from "../../color";
import { useNavigation } from "@react-navigation/native";
const Policy = () => {
  const navigation = useNavigation();
  return (
    <View style={{ flex: 1, justifyContent: "space-between" }}>
      <View style={{ height: 150, justifyContent: "center", padding: 10 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <View
        style={{
          backgroundColor: "#fff",
          padding: 20,
          elevation: 10,
          height: "80%",
          borderTopRightRadius: 15,
          borderTopLeftRadius: 15,
          justifyContent: "space-between",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            borderColor: "#d4d4d4",
            borderBottomWidth: 2,
            paddingBottom: 10,
          }}
        >
          <FontAwesome6
            name="rectangle-list"
            size={24}
            color={color.colorMain}
          />
          <AppText _3xl bold marginL>
            {i18n.t("policy")}
          </AppText>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
          <TouchableOpacity
            style={{
              ...styles.button,
              borderWidth: 1,
              borderColor: color.colorMain,
            }}
            onPress={()=>navigation.goBack()}
          >
            <AppText m main>
              {i18n.t("decline")}
            </AppText>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ ...styles.button, backgroundColor: color.colorMain }}
            onPress={()=>navigation.navigate("login")}
          >
            <AppText m white>
              {i18n.t("accept")}
            </AppText>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Policy;
const styles = StyleSheet.create({
  button: {
    width: "45%",
    padding: 7.5,
    alignItems: "center",
    borderRadius: 6,
    justifyContent: "center",
  },
});
