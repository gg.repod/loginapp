import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState,useEffect } from "react";
import AppText from "../component/AppText";
import i18n from "../languge/i18n";
import { Feather } from '@expo/vector-icons';
import color from "../../color";
import { useNavigation } from "@react-navigation/native";

const CreatePin = () => {
  const [pin, setPin] = useState("");
  const navigation = useNavigation()

  const handlePinPress = (num) => {
    if (pin.length < 6) {
      setPin(pin + num);
    }
  };

  const handleDeletePress = () => {
    setPin(pin.slice(0, -1));
  };
  useEffect(()=>{

    if(pin.length === 6){
        navigation.navigate("finger")
    }
  })
  return (
    <View style={{ alignItems: "center", justifyContent: "center", flex: 1 }}>
      <AppText xl>{i18n.t("confirmpin")}</AppText>
      <View style={styles.pinContainer}>
        {[1, 2, 3, 4, 5, 6].map((item, index) => (
          <View key={index} style={styles.pinItem}>
            {index < pin.length ? <View style={styles.pinItem2} /> : null}
          </View>
        ))}
      </View>
      <View style={{ flexDirection: "row",justifyContent:"space-around",width:"80%" }}>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(1)}>
          <AppText _4xl>1</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(2)}>
          <AppText _4xl>2</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(3)}>
          <AppText _4xl>3</AppText>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row",justifyContent:"space-around",width:"80%" ,marginTop:15}}>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(4)}>
          <AppText _4xl>4</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(5)}>
          <AppText _4xl>5</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(6)}>
          <AppText _4xl>6</AppText>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row",justifyContent:"space-around",width:"80%" ,marginTop:15}}>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(7)}>
          <AppText _4xl>7</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(8)}>
          <AppText _4xl>8</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(9)}>
          <AppText _4xl>9</AppText>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: "row",justifyContent:"space-around",width:"80%" ,marginTop:15}}>
        <View style={{width:75}}/>
        <TouchableOpacity style={styles.numPadItem} onPress={() => handlePinPress(0)}>
          <AppText _4xl>8</AppText>
        </TouchableOpacity>
        <TouchableOpacity style={{width:75,alignItems:"center",justifyContent:"center"}} onPress={handleDeletePress}>
        <Feather name="delete" size={30} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CreatePin;

const styles = StyleSheet.create({
  pinContainer: {
    flexDirection: "row",
    marginBottom: 75,
    marginTop:20
  },
  pinItem: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "gray",
    marginRight: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 40,
  },
  pinItem2: {
    width: 20,
    height: 20,
    borderWidth: 1,
    borderColor: "gray",

    justifyContent: "center",
    alignItems: "center",
    borderRadius: 40,
    backgroundColor:color.colorMain
  },

  numPadItem: {
    width: 75,
    aspectRatio: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    marginBottom: 10,
    borderRadius:100
  },
});
