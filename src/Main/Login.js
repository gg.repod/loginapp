import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";
import i18n from "../languge/i18n";
import AppText from "../component/AppText";
import { AntDesign } from "@expo/vector-icons";
import color from "../../color";
import { useNavigation } from "@react-navigation/native";

const Login = () => {
  const navigation = useNavigation()
  const [check, setCheck] = useState(false);
  const pressCheck = () => {
    setCheck(!check);
  };
  return (
    <View style={{ flex: 1, justifyContent: "center", padding: 20,paddingTop:200 }}>
      <TextInput style={styles.textInput} placeholder={i18n.t("username")} />
      <TextInput
        style={{ ...styles.textInput, marginTop: 60 }}
        placeholder={i18n.t("password")}
      />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginVertical: 25,
        }}
      >
        {/* checkbox */}
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <TouchableOpacity
            style={{
              borderWidth: 1,
              height: 17.5,
              width: 17.5,
              backgroundColor: check ? color.colorMain : "#fff",
              borderRadius: 4,
              marginRight: 5,
              borderColor: "#c6c6c6",
              alignItems: "center",
            }}
            onPress={pressCheck}
          >
            {check ? (
              <AntDesign name="check" size={14} color="white" />
            ) : (
              <View />
            )}
          </TouchableOpacity>
          <AppText gray >{i18n.t("remember")}</AppText>
        </View>

        <TouchableOpacity onPress={()=>navigation.navigate("forget")}>
          <AppText gray >{i18n.t("forget")}</AppText>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={{ ...styles.button, backgroundColor: color.colorMain }}
        onPress={()=>navigation.navigate("otp")}
      >
        <AppText white m>
          {i18n.t("login")}
        </AppText>
      </TouchableOpacity>
      <View style={{ flexDirection: "row", justifyContent: "space-around" ,marginVertical:30,alignItems:"center"}}>
        <View
          style={styles.line}
        />
        <AppText light >{i18n.t("noaccount")}</AppText>
        <View
          style={styles.line}
        />
      </View>

      <TouchableOpacity
        style={{ ...styles.button, borderWidth: 2, borderColor: "#c6c6c6" }}
      >
        <AppText main>{i18n.t("register")}</AppText>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  textInput: {
    borderBottomWidth: 1,
    borderColor: "#d4d4d4",
    fontFamily: "a",
  },
  button: {
    alignItems: "center",
    padding: 15,
    borderRadius: 6,
  },
  line:{
    height: 1,
    backgroundColor: "#c6c6c6",

    width:"35%",

  }
});
