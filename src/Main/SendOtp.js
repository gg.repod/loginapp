import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import React from "react";
import AppText from "../component/AppText";
import i18n from "../languge/i18n";
import color from "../../color";
import { useNavigation } from "@react-navigation/native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
const SendOtp = () => {
  const navigation = useNavigation();
  return (
    <View style={{ padding: 20 }}>
      <View style={{ height: 100, justifyContent: "center" }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <View style={{justifyContent:"center",height:"80%"}}>
        <Image
          source={require("../../assets/otp.png")}
          style={{
            height: 100,
            width: 100,
            tintColor: color.colorMain,
            alignSelf: "center",
          }}
        />
        <View style={{ marginTop: 50 ,alignItems:"center"}}>
          <AppText bold xxl>{i18n.t("otpsend")}</AppText>
          <AppText main marginT xxl >082-XXX-8998</AppText>
        </View>
        <TouchableOpacity
            style={{
              alignItems: "center",
              padding: 15,
              borderRadius: 6,
              backgroundColor: color.colorMain,
              marginTop: 50,
            }}
            onPress={() => navigation.navigate("verify")}
          >
               <AppText white>{i18n.t("requestotp")}</AppText>
          </TouchableOpacity>
          <AppText gray marginT c>{i18n.t("incase")}</AppText>
      </View>
    </View>
  );
};

export default SendOtp;

const styles = StyleSheet.create({});
