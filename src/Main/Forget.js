import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import React, { useState } from "react";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import AppText from "../component/AppText";
import i18n from "../languge/i18n";
import color from "../../color";
const Forget = () => {
  const navigation = useNavigation();
  const [complete, setComplete] = useState(false);

  return (
    <View
      style={
        complete
          ? { flex: 1,  justifyContent: "center", padding: 20 }
          : { flex: 1, padding: 20 }
      }
    >
      {complete ? (
        <View >
          <Ionicons
            name="checkmark-circle-outline"
            size={175}
            color={color.colorMain}
            style={{textAlign:"center"}}
          />
          <View style={{ marginTop: 50, alignItems: "center" }}>
            <AppText bold xl>
              {i18n.t("complete")}
            </AppText>
            <AppText m marginT>
              {i18n.t("reset")}
            </AppText>
          </View>
          <TouchableOpacity
            style={{
              alignItems: "center",
              padding: 15,
              borderRadius: 6,
              backgroundColor: color.colorMain,
              marginTop: 50,
            }}
            onPress={()=>navigation.navigate("login")}
          >
            <AppText white>{i18n.t("ok")}</AppText>
          </TouchableOpacity>
        </View>
      ) : (
        <View>
          <View style={{ height: 100, justifyContent: "center" }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <AntDesign name="arrowleft" size={24} color="black" />
            </TouchableOpacity>
          </View>
          <AppText bold _3xl>
            {i18n.t("forget")}
          </AppText>
          <AppText xl marginT>
            {i18n.t("plsenter")}
          </AppText>

          <TextInput
            placeholder={i18n.t("emailphone")}
            style={styles.textInput}
          />
          <TouchableOpacity
            style={{
              alignItems: "center",
              padding: 15,
              borderRadius: 6,
              backgroundColor: color.colorMain,
              marginTop: 50,
            }}
            onPress={() => setComplete(true)}
          >
            <AppText white>{i18n.t("send")}</AppText>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default Forget;

const styles = StyleSheet.create({
  textInput: {
    borderBottomWidth: 1,
    borderColor: "#d4d4d4",
    fontFamily: "a",
    marginTop: 100,
  },
});
