import { View, Text, TouchableOpacity } from "react-native";
import React, { useState, useEffect } from "react";
import AppText from "../component/AppText";
import i18n from "../languge/i18n";
import color from "../../color";
import { useNavigation } from "@react-navigation/native";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import OTPInput from "../component/OTPInput";

const Verify = () => {
  const navigation = useNavigation();
  const [count, setCount] = useState(60);
  const [otp, setOtp] = useState(Array(6).fill(''));

  const handleOtpChange = (index, value) => {
    const newOtp = [...otp];
    newOtp[index] = value;
    setOtp(newOtp);
  };


  useEffect(() => {
    const intervalId = setInterval(() => {
      setCount((count) => count - 1);
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);
  const setTimeOut = () => {
    setCount(60);
  };
  useEffect(() => {
    const isFilled = otp.every(code => code !== '');
    if (isFilled) {
      navigation.navigate('createpin');
    }
  }, [otp, navigation]);
  return (
    <View style={{ padding: 20 }}>
      <View style={{ height: 100, justifyContent: "center" }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={24} color="black" />
        </TouchableOpacity>
      </View>
      <View>
        <AppText bold _3xl>
          {i18n.t("verify")}
        </AppText>
        <AppText marginT l>
          {i18n.t("plsverify")}
        </AppText>
        <AppText marginT l>
          082-XXX-XXXX
        </AppText>
      </View>
      <View style={{marginVertical:100}}>
      <OTPInput onChangeText={handleOtpChange} />
      </View>

      <View style={{ alignItems: "center" }}>
        <AppText gray l>
          {i18n.t("ifnohave")}
        </AppText>
        <TouchableOpacity onPress={setTimeOut}>
          <AppText l main marginT>
            {i18n.t("resend")} {count > 0 && `(${count})`}
          </AppText>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Verify;
