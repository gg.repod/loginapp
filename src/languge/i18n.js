import AsyncStorage from "@react-native-async-storage/async-storage";
import en from './en.json'
import th from './th.json'
import i18n from "i18n-js"

i18n.fallbacks = true
i18n.translations = { 'en': en, 'th': th }
AsyncStorage.getItem('locale').then(locale => {
    if (locale == null) {
        AsyncStorage.setItem('locale', 'th')
        i18n.locale = 'th'
    } else {
        i18n.locale = locale
    }
})


export default i18n