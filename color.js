export default color = {
    colorMain:"#006241",
    black:"#333333",
    white: '#ffffff',
    gray:"#838282",
    blue:"#3395ff"
}