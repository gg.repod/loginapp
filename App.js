import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { LogBox } from 'react-native';
import React from 'react';
import Toast from 'react-native-toast-message';
import Main from './src/Main';

if (__DEV__) {
	LogBox.ignoreAllLogs()
	LogBox.ignoreLogs(["EventEmitter.removeListener"]);
}

export default function App() {
  return (
    <>
      <Main/>
      <Toast/>
    </>
  );
}


